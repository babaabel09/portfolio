const navtog = document.querySelector(".navbar");
const aClick = document.querySelectorAll(".nav-cont a");
const bars = document.getElementById("bars");
const mark = document.querySelector(".fa-xmark");
const navMenu = document.querySelector(".nav-cont");

const togNav = () => {
  navtog.classList.add("navbartog");
  bars.style.display = "none";
  mark.style.display = "block";
  navMenu.style.display = "block";
};

const cancel = () => {
  if (navtog.classList.contains("navbartog")) {
    navtog.classList.remove("navbartog");
    mark.style.display = "none";
    bars.style.display = "block";
    navMenu.style.display = "none";
  }
};

aClick.forEach((item) => {
  item.addEventListener("click", (e) => {
    if (navtog.classList.contains("navbartog")) {
      navtog.classList.remove("navbartog");
      navMenu.style.display = "none";
      bars.style.display = "block";
      mark.style.display = "none";
    }
  });
});

const port = () => {
  console.log("clicked");
  if (navtog.classList.contains("navbartog")) {
    navtog.classList.remove("navbartog");
    navMenu.style.display = "none";
    bars.style.display = "block";
    mark.style.display = "none";
  }
};
